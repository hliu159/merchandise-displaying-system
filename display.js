/*
题目描述
你需要完成一个用于渲染分页数据的网页显示功能。页面包含显示商品列表以及分页指示器的功能。请阅读以下代码，并按照题目要求进行实现：

1. 页面容器通过 document.getElementById('target') 获取。
2. 当前页码初始值为 1，每页显示的数据量为 12。
3. 实现 render() 函数，用于清空容器并渲染当前页面的数据列表。
    使用 slice() 方法，根据每页显示的数据量切分数组。
    每个数据项的渲染需要包含一个链接和商品的图片、标题、价格。
4. 编写 generatePagination() 函数，接收 totalItems、pageSize、currentPage 参数，并生成分页器。
    分页器应使用样式 <style> 标签设置其外观和交互效果。
5. 编写 renderPagination() 函数，实现分页器的生成逻辑：
    分页器需要始终显示第一页和最后一页的按钮。
    在页码数量少于或等于 10 时，分页器显示所有页码。
    当页码较多时，分页器中间部分使用省略号 ... 显示部分页码，始终保留前后若干页。
6. 添加事件监听器以实现分页指示器变动逻辑，当点击分页按钮时，当前页面内容及指示器需进行更新。

请仔细阅读代码并在指定位置完成所需的代码实现，使整个分页渲染功能正常工作。
*/

// 渲染页面
function display(displayData) {
  let target = document.getElementById('target'); // 页面容器
  let p = 1; // 当前页码
  const pageSize = 12; // 每页显示的数据量

  function render() {
    target.innerHTML = '';
    //Todo1: 使用slice方法按照每页数据量切分数组
    let rawDataRender = displayData.slice();
    
    rawDataRender.forEach((item) => {
      target.innerHTML += `
        <div class="col-12 col-sm-6 col-md-6 col-xl-3 space-control">
          <a href="./detail.html?prodId=${item.prodId}">
            <div class="picture-position">
              <img src="https://storage.googleapis.com/luxe_media/wwwroot/${item.productMedia[0].url}">
            </div>
            <p>${item.title}</p>
            <p>$${item.price}</p>
          </a>
        </div>
      `;
    });
  }

  function generatePagination(totalItems, pageSize, currentPage) {
    let totalPages = Math.ceil(totalItems / pageSize);
    let pagination = document.querySelector('#pagination');

    // style标签和样式
    let style = document.createElement('style');
    style.innerHTML = "#pagination{width:100%;margin-top:30px;display:flex;justify-content: center;padding-left:0;}#pageTips{position:fixed;right:10px;font-size:15px;}ul li{list-style:none;display:inline-block;user-select:none;}.list-items{width:36px;height:36px;line-height:36px;text-align:center;background-color:#fff;color:#000;cursor:pointer;transition:all .3s;border:1px solid #dedede;border-radius:5px;}.list-items:hover{background-color:#e9ecef;border-color:#dee2e6;}.active,.active:hover{color:#fff;background-color:#C8261C;border-color:#C8261C;}"
    document.getElementsByTagName('head')[0].appendChild(style);

    function renderPagination() {
      let pageHtml = '';
      pageHtml += `<li class='list-items' id='btnPrev'><i class="bi bi-caret-left"></i></li>`;

      // 始终显示第一页按钮
      pageHtml += `<li class='list-items' data-page='1'>1</li>`;

      if (totalPages <= 10) {
        for (let i = 2; i <= totalPages; i++) {
          pageHtml += `<li class='list-items' data-page='${i}'>${i}</li>`;
        }
      } else {
        if (currentPage > 5) {
          pageHtml += `<li class='list-items' id='btnGoLeft'>...</li>`;
        }

        let start = currentPage > 5 ? currentPage - 2 : 2;
        let end = currentPage < totalPages - 4 ? currentPage + 2 : totalPages - 1;

        for (let i = start; i <= end; i++) {
          if (i !== 1 && i !== totalPages) {
            pageHtml += `<li class='list-items' data-page='${i}'>${i}</li>`;
          }
        }

        if (currentPage < totalPages - 4) {
          pageHtml += `<li class='list-items' id='btnGoRight'>...</li>`;
        }

        // 始终显示最后一页按钮
        pageHtml += `<li class='list-items' data-page='${totalPages}'>${totalPages}</li>`;
      }

      pageHtml += `<li class='list-items' id='btnNext'><i class="bi bi-caret-right"></i></li>`;
      pagination.innerHTML = pageHtml;

      // 设置当前页的激活状态
      let activePage = pagination.querySelector(`li[data-page='${currentPage}']`);
      if (activePage) {
        activePage.classList.add('active');
      }
    }
  
    // 添加事件监听
    pagination.addEventListener('click', function (e) {
      let target = e.target.closest('.list-items');
      if (target) {
          //Todo2: 实现分页切换指示器变动逻辑

          render();
          renderPagination();
      }
    });
    renderPagination();
  }

  // 初始化页面
  render();
  generatePagination(displayData.length, pageSize, p);
}
