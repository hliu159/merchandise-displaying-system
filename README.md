# Merchandise Displaying System（JavaScript）

Functions  
•	Product screening: based on category or price  
•	Price sorting: ascending or descending  
•	Pagination: automatic pagination. Each page displays 12 items. You can click the button to jump to the page. If there are more than 9 pages, controls can be automatically shrunk  
  
H Liu  
December 2022  
