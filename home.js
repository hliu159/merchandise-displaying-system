let data = [];//有效数组（排除无图片数组）
let cateData = [];//类别筛选信息
let valueData = [];//价格筛选信息
let categoryId = 0;//类别筛选属性
let priceRange = 0;//价格筛选属性

initial();

//初始化
function initial(){

//将有图片的数组装入data数组
for (let item of rawdata) {
  if(item.productMedia[0]){
    data.push(item);
  }
}

//加载类别选择器
let cateDataInfo = [];//将所有类别信息装入此数组
let cate = document.getElementById('cateSelect'); //类别选择器
cateDataInfo = data[0].prodType.productCategory;
for (let item of cateDataInfo) {
  cate.innerHTML += `
  <option value=${item.categoryId}>${item.categoryName}</option>
  `;
}

//读取url信息
  if(getUrlParam('categoryId')){
    categoryId = parseInt(getUrlParam('categoryId'));
  }

  if(getUrlParam('priceRange')){
    priceRange = parseInt(getUrlParam('priceRange'));
  }

  let sort = 0; //创建排序变量
  if(getUrlParam('sort')){
    sort = parseInt(getUrlParam('sort'));
  }

//初始化加载数组
  cateData = data;
  valueData = data;
  if(categoryId !=0 || priceRange !=0){
    filterCateData();
    filterValueData();
    document.getElementById('cateSelect').value = categoryId;
    document.getElementById('valueSelect').value = priceRange;
  } else{
    display(data);
  }

  //判断排序方式
  if(sort == 1){
    priceSortUp();
  }
  if(sort == 2){
    priceSortDown();
  }
}

//URL解析器
function getUrlParam(paramName){
  const url = window.location.search.substring(1);
  const urlParams = new URLSearchParams(url);
  return urlParams.get(paramName);
}

//选择器
function changeCategoryId(){
  let cate = document.getElementById('cateSelect'); //类别选择器
  categoryId = cate.value;
  filterCateData();
  filterValueData();
}

function changePriceRangeId(){
  let value = document.getElementById('valueSelect');//价格选择器
  priceRange = value.value;
  filterValueData();
}

//类别筛选
  function filterCateData(){
    cateData = [];
    if (categoryId == 0){
      cateData = data;
    } else {
    for (let item of data) {
      if(item.categoryId == categoryId){
        cateData.push(item);
      }
    }
  }
  } ;

//价格筛选
  function filterValueData(){
    valueData = cateData;
    if (priceRange != 0){
      valueData= [];
    for (let item of cateData) {
      if(priceRange == 1 && item.price <= 100){
      valueData.push(item);
    }
      if(priceRange == 2 && item.price > 100 && item.price <= 500){
      valueData.push(item);
    }
      if(priceRange == 3 && item.price > 500 && item.price <= 1000){
      valueData.push(item);
    }
      if(priceRange == 4 && item.price > 1000){
      valueData.push(item);
    }
  }
}
    filterData();
  };

//最终筛选
  function filterData(){
    if (categoryId == 0){
      history.replaceState(200, Document, "home.html?priceRange="+priceRange);
    } else if (priceRange == 0){
      history.replaceState(200, Document, "home.html?categoryId="+categoryId);
    } else{
      history.replaceState(200, Document, "home.html?categoryId="+categoryId+"&priceRange="+priceRange);
    }

    if (categoryId == 0 && priceRange == 0){
      history.replaceState(200, Document, "home.html");
    } 

    // let filterData = [];
    // filterData = valueData.filter(item=> cateData.indexOf(item) !== -1);
    display (valueData);
  }

//价格排序
    function priceSortUp(){ 
      if (categoryId == 0){
        history.replaceState(200, Document, "home.html?priceRange="+priceRange+"&sort=1");
      } else if (priceRange == 0){
        history.replaceState(200, Document, "home.html?categoryId="+categoryId+"&sort=1");
      } else{
        history.replaceState(200, Document, "home.html?categoryId="+categoryId+"&priceRange="+priceRange+"&sort=1");
      }

      if (categoryId == 0 && priceRange == 0){
      history.replaceState(200, Document, "home.html?&sort=1");
      }
      display(valueData.sort((a,b) => a.price - b.price));
    }

    function priceSortDown(){  
      if (categoryId == 0){
        history.replaceState(200, Document, "home.html?priceRange="+priceRange+"&sort=2");
      } else if (priceRange == 0){
        history.replaceState(200, Document, "home.html?categoryId="+categoryId+"&sort=2");
      } else{
        history.replaceState(200, Document, "home.html?categoryId="+categoryId+"&priceRange="+priceRange+"&sort=2");
      }

      if (categoryId == 0 && priceRange == 0){
      history.replaceState(200, Document, "home.html?&sort=2");
      }
      display(valueData.sort((a,b) => b.price - a.price));
    }

//清空所有筛选
  function clearFilter(){
    window.location.replace("./home.html");
  }

// 渲染页面
function display(displayData) {
  let target = document.getElementById('target'); // 页面数据容器
  let p = 1; // 当前页码
  const pageSize = 12; // 每页显示的数据量

  // 渲染当前页数据的函数
  function render() {
    target.innerHTML = ''; // 清空当前容器内容

    // Todo1: 使用 slice 方法根据当前页码和每页数据量切分数据数组
    let rawDataRender = displayData.slice((p - 1) * pageSize, p * pageSize);

    // 遍历切分后的数据数组，创建每个项目的 HTML 内容并添加到容器中
    rawDataRender.forEach((item) => {
      target.innerHTML += `
        <div class="col-12 col-sm-6 col-md-6 col-xl-3 space-control">
          <a href="./detail.html?prodId=${item.prodId}">
            <div class="picture-position">
              <img src="https://storage.googleapis.com/luxe_media/wwwroot/${item.productMedia[0].url}">
            </div>
            <p>${item.title}</p>
            <p>$${item.price}</p>
          </a>
        </div>
      `;
    });
  }

  // 生成分页控件和处理分页逻辑的函数
  function generatePagination(totalItems, pageSize, currentPage) {
    let totalPages = Math.ceil(totalItems / pageSize); // 计算总页数
    let pagination = document.querySelector('#pagination'); // 获取分页控件容器

    // 创建 <style> 标签来添加自定义分页样式
    let style = document.createElement('style');
    style.innerHTML = `
      #pagination {
        width: 100%;
        margin-top: 30px;
        display: flex;
        justify-content: center;
        padding-left: 0;
      }
      #pageTips {
        position: fixed;
        right: 10px;
        font-size: 15px;
      }
      ul li {
        list-style: none;
        display: inline-block;
        user-select: none;
      }
      .list-items {
        width: 36px;
        height: 36px;
        line-height: 36px;
        text-align: center;
        background-color: #fff;
        color: #000;
        cursor: pointer;
        transition: all .3s;
        border: 1px solid #dedede;
        border-radius: 5px;
      }
      .list-items:hover {
        background-color: #e9ecef;
        border-color: #dee2e6;
      }
      .active, .active:hover {
        color: #fff;
        background-color: #C8261C;
        border-color: #C8261C;
      }
    `;
    document.getElementsByTagName('head')[0].appendChild(style); // 将样式标签添加到页面 <head>

    // 渲染分页控件的函数
    function renderPagination() {
      let pageHtml = '';

      // 添加“上一页”按钮
      pageHtml += `<li class='list-items' id='btnPrev'><i class="bi bi-caret-left"></i></li>`;

      // 始终显示第一页按钮
      pageHtml += `<li class='list-items' data-page='1'>1</li>`;

      // 如果总页数小于等于10，显示所有页码
      if (totalPages <= 10) {
        for (let i = 2; i <= totalPages; i++) {
          pageHtml += `<li class='list-items' data-page='${i}'>${i}</li>`;
        }
      } else {
        // 如果总页数大于10，动态调整显示范围

        // 如果当前页码大于5，显示“...”省略标记
        if (currentPage > 5) {
          pageHtml += `<li class='list-items' id='btnGoLeft'>...</li>`;
        }

        // 设置显示的页码范围，当前页两侧各显示两页
        let start = currentPage > 5 ? currentPage - 2 : 2;
        let end = currentPage < totalPages - 4 ? currentPage + 2 : totalPages - 1;

        // 循环生成范围内的页码
        for (let i = start; i <= end; i++) {
          if (i !== 1 && i !== totalPages) {
            pageHtml += `<li class='list-items' data-page='${i}'>${i}</li>`;
          }
        }

        // 如果当前页小于总页数减去4，显示“...”省略标记
        if (currentPage < totalPages - 4) {
          pageHtml += `<li class='list-items' id='btnGoRight'>...</li>`;
        }

        // 始终显示最后一页按钮
        pageHtml += `<li class='list-items' data-page='${totalPages}'>${totalPages}</li>`;
      }

      // 添加“下一页”按钮
      pageHtml += `<li class='list-items' id='btnNext'><i class="bi bi-caret-right"></i></li>`;

      // 设置分页控件的 HTML 内容
      pagination.innerHTML = pageHtml;

      // 设置当前页的激活状态
      let activePage = pagination.querySelector(`li[data-page='${currentPage}']`);
      if (activePage) {
        activePage.classList.add('active');
      }
    }

    // 为分页按钮添加点击事件监听
    pagination.addEventListener('click', function (e) {
      let target = e.target.closest('.list-items'); // 确保点击的是分页项
      if (target) {
        // Todo2: 实现分页切换的逻辑
        if (target.id === 'btnPrev' && currentPage !== 1) {
          currentPage--; // 如果点击的是“上一页”且当前页不是第一页，则页码减1
        } else if (target.id === 'btnNext' && currentPage != totalPages) {
          currentPage++; // 如果点击的是“下一页”且当前页不是最后一页，则页码加1
        } else if (target.hasAttribute('data-page')) {
          currentPage = parseInt(target.getAttribute('data-page')); // 获取点击的页码并转换为数字
        } else {
          return; // 如果点击无效，则不进行任何操作
        }

        // 更新当前页码，重新渲染内容和分页控件
        p = currentPage;
        render();
        renderPagination();
      }
    });

    // 初始化渲染分页控件
    renderPagination();
  }

  // 初始化页面内容和分页控件
  render();
  generatePagination(displayData.length, pageSize, p);
}

/*
e.target.closest('.list-items')：
e.target 是触发点击事件的最具体的元素。
closest 方法用于获取调用它的元素或从调用它的元素开始向上遍历其祖先元素，直到找到匹配选择器的最近的元素。如果找到，它返回该元素；如果找不到，则返回 null。
在这段代码中，用于确保用户点击的是具有 list-items 类的分页按钮或其子元素。

target.hasAttribute('data-page')：
hasAttribute 方法用于检查元素是否有指定的属性。
在这段代码中，用于检测 target 是否具有 data-page 属性，这表明该元素是一个页码按钮。

target.getAttribute('data-page')：
getAttribute 方法用于获取指定属性的值。
在这段代码中，获取 data-page 属性的值并将其转换为整数，用于更新 currentPage 变量。
*/