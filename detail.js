let prodId = parseInt(getUrlParam('prodId'));
let currentProduct = getProduct(prodId);
let title = currentProduct.title;
let img = currentProduct.productMedia[0].url;
let description = currentProduct.description;
let price = currentProduct.price;

function getProduct(ID){
  let target;
  for (let prod of rawdata){
    if (prod.prodId == ID){
      target = prod
    }
  }
  return target;
}

function getUrlParam(paramName){
  const url = window.location.search.substring(1);
  const urlParams = new URLSearchParams(url);
  return urlParams.get(paramName);
}

// 添加到页面
const prod = `
<h5>${title}</h5>
<hr>
<div class="col-12 col-sm-12 col-md-12 col-xl-6 space-control">
<img src="https://storage.googleapis.com/luxe_media/wwwroot/${img}">
</div>
<div class="col-12 col-sm-12 col-md-12 col-xl-6 space-control">
<p>${description}</P>
<p>$${price}</p>
</div>
`

const target = document.getElementById('target');
target.innerHTML = prod;
